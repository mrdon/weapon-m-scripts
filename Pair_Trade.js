/**
 *
 * Trades a pair of ports down to 20%
 *
 * @accelerator control T
 *
 */

importClass(Packages.krum.weaponm.database.Product);

var lastSector;
var sec1, sec2;

function areSectors(sec1, sec2) {
    return {
        pairOf : function(prod1, prod2) {
            let p1 = sec1.port;
            let p2 = sec2.port;
            if ((p1.sells(prod1) && p2.buys(prod1) &&
                 p1.buys(prod2) && p2.sells(prod2)) ||
                (p1.buys(prod1) && p2.sells(prod1) &&
                 p1.sells(prod2) && p2.buys(prod2))) {

                if (p1.status != null && p2.status != null) {
                    return Math.min(
                        Math.min(p1.getPercent(prod1), p2.getPercent(prod1)),
                        Math.min(p1.getPercent(prod2), p2.getPercent(prod2)));
                } else {
                    return 50;
                }
            }
            return -1;
        }
    }
}

function onStart(script) {
    script.isConnected() || quit("Must be connected");
    script.currentPrompt.name() == "COMMAND_PROMPT" || quit("Must be at a command prompt");

    sec1 = script.database.you.sector;
    var port1 = sec1.port;

    var maxPerc = -1;
    sec1.warpsOut.forEach(function(warpId) {
        let warp = script.sector(warpId);
        if (warp.port) {
            let perc = areSectors(sec1, warp).pairOf(Product.ORGANICS, Product.EQUIPMENT);
            if (perc > maxPerc) {
                sec2 = warp;
                maxPerc = perc;
            }    
        }
    });

    sec1 && sec2 || quit("No pair ports found");

    maxPerc > 20 || quit("Not enough product");

    print("Chose start: " + sec1.number + " end: " + sec2.number + " max perc: " + maxPerc);

    send("pt");
}

var quittingMessage;
function moveToNextSector() {
    let nextSector = db.you.sector === sec1 ? sec2 : sec1;
    send(nextSector.number + "*");
}

function onCommandPrompt(time, sector) {
    if (quittingMessage) {
        printAnsi(quittingMessage);
        quit();
    } else if (lastSector == sector) {
        moveToNextSector();
    } else {
        send("pt");
    }
}

var curPerc;
var percMod;
var isFinalOffer;
function onTradeInitPrompt(product, sell) {
    if (quittingMessage || product == Product.FUEL_ORE) {
        send("0*");
    } else if (db.you.sector.port.getPercent(product) < 20) {
        send("0*");
        quittingMessage = "**@|red << Not enough goods >>|@*";
    } else {
        send("*");
        isFinalOffer = false;
        percMod = sell ? 1.1 : -1.1;
        curPerc = 100 + (sell ? -5 : 5) ;
    } 
}

function onTradeOfferPrompt(credits) {
    if (isFinalOffer) curPerc += percMod;
    let price = parseInt((credits * curPerc) / 100);
    send(price + "*");
    curPerc += percMod;
}

function onTradeNothing() {
    lastSector = db.you.sector;
}

function onFinalOffer() {
    isFinalOffer = true;
}

function onTradeAccepted() {
    lastSector = db.you.sector;
}