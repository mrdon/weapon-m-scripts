/**
 *
 * Opens a chat dialog to display and send fedcom messages
 *
 * @accelerator control D
 */
importPackage(Packages.javax.swing);
importPackage(java.awt.event);

var script;
var dialog;
var outputArea;

function onStart(scr) {
    script = scr;
    dialog = script.createDialog(false);

    // bunch of swing setup stuff
    var splitter = new JSplitPane();
    var outputPanel = new JPanel();
    var outputScroll = new JScrollPane();
    outputArea = new JTextArea();
    var chatPanel = new JPanel();
    var chatScroll = new JScrollPane();
    var chatInput = new JTextArea();

    dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    splitter.setOrientation(JSplitPane.VERTICAL_SPLIT);

    outputPanel.setPreferredSize(new java.awt.Dimension(398, 220));

    outputArea.setColumns(20);
    outputArea.setRows(5);
    outputArea.setEditable(false);
    outputArea.setLineWrap(true);
    outputScroll.setViewportView(outputArea);

    var outputPanelLayout = new GroupLayout(outputPanel);
    outputPanel.setLayout(outputPanelLayout);
    outputPanelLayout.setHorizontalGroup(
        outputPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(outputScroll, GroupLayout.DEFAULT_SIZE, 398, java.lang.Short.MAX_VALUE)
    );
    outputPanelLayout.setVerticalGroup(
        outputPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(outputScroll, GroupLayout.DEFAULT_SIZE, 220, java.lang.Short.MAX_VALUE)
    );

    splitter.setTopComponent(outputPanel);

    chatPanel.setPreferredSize(new java.awt.Dimension(398, 50));

    chatScroll.setViewportView(chatInput);

    var bottomPanelLayout = new GroupLayout(chatPanel);
    chatPanel.setLayout(bottomPanelLayout);
    bottomPanelLayout.setHorizontalGroup(
        bottomPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(chatScroll, GroupLayout.DEFAULT_SIZE, 398, java.lang.Short.MAX_VALUE)
    );
    bottomPanelLayout.setVerticalGroup(
        bottomPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(chatScroll, GroupLayout.DEFAULT_SIZE, 73, java.lang.Short.MAX_VALUE)
    );

    splitter.setRightComponent(chatPanel);

    var layout = new GroupLayout(dialog.getContentPane());
    dialog.getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(splitter, GroupLayout.DEFAULT_SIZE, 400, java.lang.Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(splitter, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 300, java.lang.Short.MAX_VALUE)
    );

    // register chat send on enter
    chatInput.setLineWrap(true);
    chatInput.addKeyListener(new KeyAdapter({
        keyPressed : function(e) {
            if (e.getKeyCode() == e.VK_ENTER) {
                e.consume();
                var txt = chatInput.getText();
                script.sendText("`" + txt + "\r\n");
                chatInput.setText("");
                addToChat("Me", txt);    
            }
        }
    }));

    dialog.title = "Federation Comm-Link";
    dialog.pack();
    dialog.setVisible(true);

    // dispose on close
    dialog.addWindowListener(new java.awt.event.WindowAdapter({
        windowClosing : function(evt) {
            "use strict";
            print("Unloading");
            dialog.dispose();
            quit();
        }
    }));
}

function onEnd(script) {
    dialog.dispose();
}

function onChatMessage(type, sender, message) {
    if (type.name() == "FED_COMM") {
        addToChat(sender.getName(), message);

    }
}

// Adds a chat message to the output window
function addToChat(trader, message) {
    SwingUtilities.invokeLater(function() {
        outputArea.append("[" + trader + "] " + message + "\n");
    });
}
